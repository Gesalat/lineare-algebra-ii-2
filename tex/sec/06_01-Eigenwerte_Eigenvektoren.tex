\section{Eigenwerte und Eigenvektoren}
Sei 
\(D = S^{-1} \cdot A \cdot S = 
  = \begin{bmatrix}
    \lambda_1 &  & 0\\
    & \ddots & \\
    0  & \lambda_n\\
    \end{bmatrix}
\)
\[
  \begin{array}{l@{=}l@{=}l}
    AS & A \cdot [s_1\hdots s_n] & [A\cdot s_1, \hdots, A\cdot s_n]\\
    SD & [s_1 \hdots s_n] \cdot D & [\lambda_1 s_1, \hdots, \lambda_n s_n]\\
  \end{array}
  \Bigg\}
  \Rightarrow As_j =
  \underbrace{\lambda_j}_{\text{\footnote{Eigenwert}}}\underbrace{s_j}_{\text{\footnote{Eigenvektor}}},
  j = 1, \hdots, n
\]

\begin{definition}
\begin{enumerate}
  \item Sei \(f \in END(V)\). Dann heißt \(\lambda \in K\) ein
    Eigenwert von \(f\), falls es \(v \in V\backslash\{0\}\) gibt, so
    dass \(f(v) = \lambda(v)\). Der Vektor \(v\) heißt Eigenvektor von
    \(f\) zu \(\lambda\). 
    \[Sp(f) := \left\{\lambda \in K \mid \lambda - \text{Eigenwert von
        } f\right\} - \text{Spektrum von } f\]
  \item Sei \(A \in K^{n \times n}\). Dann heißt \(\lambda \in K\) ein
    \emph{Eigenwert} von \(A\), falls es einen Vektor \(v \in
    K^n\backslash \{0\}\) gibt, so dass \(Av = \lambda v\). \(v\)
    heißt \emph{Eigenvektor} von \(A\) zum Eigenwert \(\lambda\). 
    \[Sp(A) := \left\{\lambda \in K \mid \lambda - \text{Eigenwert von
        } A\right\} - \text{Spektrum von } A\]
  \item Eine Matrix \(A \in K^{n \times n}\) heißt diagonalisierbar,
    falls es eine invertierbare Matrix \(S \in K^{n \times n}\) gibt,
    so dass 
    \[S^{-1} \cdot A \cdot S = 
      \begin{bmatrix}
        \lambda_1 &  & 0\\
        & \ddots & \\
        0  && \lambda_n\\
      \end{bmatrix}
    \]
    mit \(\lambda_1, \hdots, \lambda_n \in K\). 
  \item Eine Abbildung \(f \in END(V)\) heißt diagonalisierbar, falls
    es eine Basis \(B\) von \(V\) gibt, so dass \([f]_B^B\) diagonal
    ist.
\end{enumerate}
\end{definition}
\begin{remark}\footnote{``Eine sehr schöne Frage für eine mündliche
      Prüfung''} \(v = 0\) wird als Eigenvektor ausgeschlossen, da
  sonst
\[\forall \lambda \in K: A\cdot 0 = \lambda 0, f(0) = \lambda 0 \]
\end{remark}
\begin{theorem}
Die Matrix \(A \in K^{n\times n}\) heißt genau dann diagonalisierbar,
wenn es eine Basis \(B = (v_1, \hdots, v_n)\) von \(K^n\) aus
Eigenvektoren von \(A\) gibt.\\
Sind \(\lambda_1, \hdots, \lambda_n\) die zugehörigen Eigenwerte, dh
\( Av_j = \lambda_j v_j\) für \(j = 1, \dots, n\) und 
\(S = [v_1, \hdots, v_n]\), dann gilt
\[S^{-1} \cdot A \cdot S = 
  \begin{bmatrix}
    \lambda_1 &  & 0\\
    & \ddots & \\
    0  && \lambda_n\\
  \end{bmatrix}
\]
\begin{proof}
  \begin{itemize}
    \item[\(\Rightarrow:\)] Sei \(A\) diagonalisierbar \(\Rightarrow
      \exists S \in K^{n \times n}\) invertierbar, mit 
      \(S^{-1} A S = 
      \begin{bmatrix}
        \lambda_1 &  & 0\\
        & \ddots & \\
        0  && \lambda_n\\
      \end{bmatrix} 
      \Rightarrow \) für \(S = [v_1, \dots, v_n]\) gilt \(Av_j =
      \lambda_j v_j, j = 1, \dots, n 
      \Rightarrow B = (v_1, \hdots \sigma v_n)\) eine Basis von \(V\)
    \item [\(\Leftarrow:\)] Sei \((v_1, \hdots, v_n)\) eine Basis von
      \(K^n\) aus EV \(\Rightarrow Av_j = \lambda_j v_j, j=1, \hdots,
      n \Rightarrow A\underbrace{[v_1, \hdots, v_n]}_S = S 
      \begin{bmatrix}
        \lambda_1 &  & 0\\
        & \ddots & \\
        0  && \lambda_n\\
      \end{bmatrix}
      \Rightarrow S^{-1}AS = 
      \begin{bmatrix}
        \lambda_1 &  & 0\\
        & \ddots & \\
        0  && \lambda_n\\
      \end{bmatrix}
      \)
  \end{itemize}
\end{proof}
\end{theorem}

\begin{example}
\begin{enumerate}
  \item \(f: \mathbb{R}^2 \rightarrow \mathbb{R}^2, f(z) = Az\) mit
    \(A = 
    \begin{bmatrix}
      0 & 1\\
      1 & 0\\
    \end{bmatrix}
    \)\\
\begin{center}
	\includegraphics[width=.35\textwidth]{img/06-01-eigenvektoren.pdf}
\end{center}
    \begin{enumerate}
      \item \(u = \begin{bmatrix}u_1\\u_1\\\end{bmatrix}, u_1 \in
      \mathbb{R}\) 
      \[f(u) = Au = 
        \begin{bmatrix}0 & 1\\ 1&0\\\end{bmatrix} 
        \begin{bmatrix} u_1 \\ u_1\\\end{bmatrix} 
        = \begin{bmatrix} u_1\\u_1\\\end{bmatrix} = 1 \cdot u 
        \Rightarrow \lambda = 1 - 
        \text{ EW \footnote{Eigenwert} von } \underset{(A)}{f}  
        u = \begin{bmatrix}u_1\\u_1\\\end{bmatrix} - \text{ EV }
      \]
      \item \(w = \begin{bmatrix}-w_1\\w_1\\\end{bmatrix}, w_1 \in
        \mathbb{R}\) 
        \[f(w) = Aw = 
          \begin{bmatrix}0 & 1\\ 1&0\\\end{bmatrix} 
          \begin{bmatrix} -w_1 \\ w_1\\\end{bmatrix}
          =\begin{bmatrix} w_1 \\ -w_1\\\end{bmatrix} 
          = (-1) \cdot w
          \Rightarrow w - \text{ EV von } A \text{ zum EW } A = -1\]
      \item \(v_1 = \begin{bmatrix}1\\1\\\end{bmatrix},
        v_2 = \begin{bmatrix}-1\\1\\\end{bmatrix},
        S = \begin{bmatrix}v_1 & v_2\end{bmatrix} 
        = \begin{bmatrix} 1& -1\\ 1 & 1\\\end{bmatrix},
        S^{-1} = \frac{1}{2}\begin{bmatrix}1 & 1\\ -1 &
          1\\\end{bmatrix}\)
        \[S^{-1} A S =\frac{1}{2}
          \begin{bmatrix}1 & 1\\ -1 & 1\\\end{bmatrix}
          \begin{bmatrix}0 & 1\\ 1 & 0\\\end{bmatrix}
          \begin{bmatrix} 1 & -1\\ 1 & 1\\\end{bmatrix}
          = \begin{bmatrix} 1 & 0\\ 0 & -1\\\end{bmatrix}
          \Rightarrow A \text{ diagonalisierbar}
        \]
    \end{enumerate}
\item \(F = \begin{bmatrix} 0 & 1\\ 0 & 0\\\end{bmatrix}\)
  Sei \(S = \begin{bmatrix} a & b\\ c & d\\\end{bmatrix} 
  \Rightarrow S^{-1} = \frac{1}{ad-bc}
  \begin{bmatrix} d & -b\\ -c & a\\\end{bmatrix}\)
  \begin{align*} S^{-1} F S = \frac{1}{ad-bc}  
    \begin{bmatrix} d & -b\\ -c &  a\\\end{bmatrix}
    \begin{bmatrix} 0 & 1\\ 0 & 0\\\end{bmatrix}
    \begin{bmatrix} a & b\\ c & d\\\end{bmatrix}
    &= S^{-1} F S = \frac{1}{ad-bc}  
    \begin{bmatrix} d & -b\\ -c &  a\\\end{bmatrix}
    \begin{bmatrix} c & d\\ 0 & 0\\\end{bmatrix}\\
    &= S^{-1} F S = \frac{1}{ad-bc}  
   \begin{bmatrix} cd & d^2 \\ -c^2 & -cd\\\end{bmatrix}\\
   &= \begin{bmatrix} * & 0\\ 0 & *\\\end{bmatrix}
   \Rightarrow c = d = 0 \Rightarrow F \text{ nicht diagonalisierbar}
 \end{align*}
\end{enumerate}
\end{example}

\begin{remark}
Ist \(B = (v_1, \dots, v_n)\) Basis von \(V\) und \(f\in END(V)
\xRightarrow{S 4.4} f = \varphi_B^{-1}\circ [f] \circ \varphi_B\)
\begin{center}
	\includegraphics[width=.3\textwidth]{img/06-02-komm_diagramm.pdf}
\end{center}
\begin{align*}
  f(v) = \lambda v 
  &\Leftrightarrow 
    \left(\varphi_B^{-1}\circ [f] \circ \varphi_B\right)(v)= \lambda v\\
  &\Leftrightarrow\begin{array}{ c }
      \left( [f]^B_B \circ \varphi_B \right) (v) = \lambda \underbrace{\varphi_B(v)}_w\cr
      [f]^B_B\underbrace{\varphi_B(v)}_w
    \end{array}
  \Bigg\}[f]_B^B w = \lambda w\\
  &\Rightarrow v \text{ ist ein EV von } f \text{ zum EW }\lambda\\
  &\Leftrightarrow \varphi_B(v) \text{ ist ein EV von } [f]_B^B \text{
    zum EW } \lambda
\end{align*}
\end{remark}

\begin{theorem}
  Die Eigenwerte von \(A \in K^{n \times n}\) sind invariant unter
  Ähnlichkeit.
\begin{proof}
Seien \(A, \tilde{A} \in K^{n \times n}\) ähnlich \(\Rightarrow
\exists S \in K^{n \times n}\) mit \(\tilde{A} = S^{-1} A S\)\\
Dann gilt: 
\begin{align*}
  \tilde{A}v = \lambda v 
  &\Leftrightarrow S^{-1} A S v = \lambda v\\
  &\Leftrightarrow A S v = \lambda S v
\end{align*}

\end{proof}
\end{theorem}

\begin{question}
\begin{enumerate}
\item Wie finden wir Eigenwerte ? 
\item Wie finden wir Eigenvektoren zu einem gegebenen Eigenwert ?
\end{enumerate}
\end{question}

\begin{observation}
\begin{align*}
  Av = \lambda v
  &\Leftrightarrow 0 = Av-\lambda v = (A - \lambda I)v\\
  &\Leftrightarrow v \in Kern(A - \lambda I)
\end{align*}
\end{observation}

\begin{definition}
Sei \(A \in K^{n \times n}\) und \(\lambda \in K\) ein Eigenvektor von
\(A\)
\begin{enumerate}
\item \(Eig_\lambda (A) := Kern(A - \lambda I)\) heißt
  \emph{Eigenraum} von \(A\) zu \(\lambda\)
\item \(g(\lambda) := dim(Eig_\lambda (A))\) heißt \emph{geometrische
      Vielfachheit von \(\lambda\)}
\end{enumerate}
\end{definition}

\begin{remark}
\begin{enumerate}
\item \(v \in Eig_\lambda(A)\backslash\{0\} \Leftrightarrow v\) ist
  Eigenvektor von \(A\) zu \(\lambda\) \(\Rightarrow\) Eine Basis von
  \(Eig_\lambda(A)\) können wir durch Lösen des LGS 
  \((A - \lambda I)x = 0\)
\item \(Eig_{\lambda_1} (A) \cap Eig_{\lambda_2} = \{0\}\) für
  \(\lambda_1 \neq \lambda_2\), denn: 
  \begin{align*}
    v \in Eig_{\lambda_1} (A) \cap Eig_{\lambda_2}(A)
    &\Rightarrow v \in Eig_{\lambda_1} \land v\in Eig_{\lambda_2}\\
    &\Rightarrow \lambda_1 v = Av = \lambda_2 v\\
    &\Rightarrow 0 = (\lambda_1 - \lambda_2)v\\
    &\Rightarrow v = 0 \text{ da } \lambda_1 \neq \lambda_2
  \end{align*}
\end{enumerate}
\end{remark}

\begin{theorem}
Seien \(A \in K^{n \times n}\) und \(v_1, \hdots, v_m \in K^n\) EV zu
paarweise verschiedenen EW \(\lambda_1, \hdots, \lambda_m \in K\) von
\(A\). Dann sind \(v_1, \dots, v_m\) linear unabhängig.

\begin{proof}
Induktion nach \(m\).
\begin{itemize}
\item \(m = 1\): \(v_1 \neq 0\) EV von \(A\) zu \(\lambda_1
  \Rightarrow v_1\) linear unabhängig. 
\item \(m-1 \rightarrow m\): Angenommen \(v_1, \hdots, v_m\) linear
  abhängig 
\begin{align*}
  &\Rightarrow \exists \mu_1, \hdots, \mu_m \in K: \sum_{j=1}^m
    \mu_jv_j = 0\\
  &\Rightarrow 
    \begin{array}{l@{=}l@{=}l}
      0 
      & A\cdot \sum_{j=1}^m \mu_j v_j 
      & \sum_{j=1}^m\mu_jAv_j = \sum_{j=1}^m\mu_j\lambda_jv_j
        \cr
      0
      & \lambda_m \sum_{j=1}^m\mu_jv_j 
      & \sum_{j=1}^m\mu_j\lambda_mv_j
    \end{array}
    \Bigg\}\\
  &\Rightarrow 0 = \sum_{j=1}^m \mu_j(\lambda_j - \lambda_m)
    = \sum_{j=1}^m \mu_j(\lambda_j - \lambda_m) v_j\\
  &\Rightarrow \mu_j (\lambda_j v_j) = 0 \, j = 1, \hdots, m-1\\
  &\Rightarrow \mu_j = 0, j = 1, \hdots, m -1\\
  &\Rightarrow 0 = \sum_{j=1}^m \mu_j v_j = \mu_m v_m\\
  &\Rightarrow \mu_m = 0 \text{ da } v_m \neq 0\\
  &\Rightarrow v_1, \hdots, v_m \text{ linear unabhängig}
\end{align*}
\end{itemize}
\end{proof}
\end{theorem}

\begin{corollary}
Hat \(A \in K^{n \times n} n\) paarweise verschiedene Eigenwerte, so
ist \(A\) diagonalisierbar
\begin{proof}
folgt aus Satz 6.3 und Satz 6.1
\end{proof}
\end{corollary}

\begin{observation}
\(\lambda \in K\) ist EW von \(A \in K^{n \times n}\)
\begin{align*}
  &\Leftrightarrow \exists v \neq 0: Av = \lambda v\\
  &\Leftrightarrow v \in Kern(A - \lambda I)\backslash\{0\}\\
  &\Leftrightarrow det(A - \lambda I) = 0 (\text{oder } det(\lambda I
    - A))
\end{align*}
\[\left( det(A-\lambda I) \neq det(\lambda I - A) 
    = det((-1)(A-\lambda I)) = (-1)^n det(A - \lambda I)\right)\]
\end{observation}

\begin{example}
\begin{enumerate}
\item \(A = \begin{bmatrix} 7 & 4\\ -8 & -5\\\end{bmatrix}\)
\begin{align*}
  det(\lambda I - A) 
  &= det\left(
    \begin{bmatrix}
      \lambda - 7 & -4\\
      8 & \lambda + 5\\
    \end{bmatrix}
  \right)\\
  &= (\lambda - 7)(\lambda + 5) + 32\\
  &= \lambda^2 - 2\lambda -3 = 0\\
  &\Rightarrow \lambda_1 = 3, \lambda_2 = -1 \text{ EW}
\end{align*}
\[
  \begin{array}{c@{,}r@{\Rightarrow}l}
    (A - \lambda_1 I)v_1 = 0
    &\begin{bmatrix}
      4 & 4\\
      -8&-8\\
    \end{bmatrix} v_1 = 0
    &Eig_3(A) = Span\left(
      \begin{bmatrix}
        1\\ -1\\
      \end{bmatrix}
    \right)\\
    (A - \lambda_2 I) v_2 = 0
    &\begin{bmatrix}
      8 & 4\\
      -8&-4\\
    \end{bmatrix}
    v_2 = 0
    &Eig_{-1}(A) = Span\left(
      \begin{bmatrix}
        1\\ -2\\
      \end{bmatrix}
    \right)\\
    S = 
    \begin{bmatrix}
      1 & 1\\
      -1&-2\\
    \end{bmatrix}
    & S^{-1} =
      \begin{bmatrix}
        2 & 1\\
        -1& -1\\
      \end{bmatrix}
    & S^{-1}AS = D = 
      \begin{bmatrix}
        3 & 0\\
        0 & -1\\
      \end{bmatrix}
  \end{array}
\]
\item \(F = 
  \begin{bmatrix}
    0 & 1\\
    0 & 0\\
  \end{bmatrix}
\)
\begin{align*}
  det(\lambda I - F) 
  &=det\left(
    \begin{bmatrix}
      \lambda & -1\\
      0 & \lambda\\
    \end{bmatrix} 
  \right)
  = \lambda^2 = 0\\
  \lambda_1 
  &= \lambda_2 = 0\\
  (F - 0I)v &= 0\\
  &\Rightarrow 
    \begin{bmatrix}
      \lambda & -1\\
      0 & \lambda\\
    \end{bmatrix}
  v = 0\\
  &\Rightarrow Eig_0(F) = Span \left(
    \begin{bmatrix}1\\0\\\end{bmatrix}
  \right)\\
  g(0) &= 1
\end{align*}
\item 
\begin{align*}
  &\begin{array}{c@{,}r@{\Rightarrow}l}
    M = 
    \begin{bmatrix}
      0 & 1\\
      -1& 0\\
    \end{bmatrix}
    \in \mathbb{R}^{2 \times 2}
    & det(\lambda I - M) 
      = det\left(
      \begin{bmatrix}
        \lambda & -1\\
        1 & \lambda\\
      \end{bmatrix}
     \right)
    = \lambda^2 +1 = 0 
    & \lambda_1 \notin \mathbb{R}
      \lambda_2 \notin \mathbb{R}
  \end{array}\\
  &\Rightarrow M \text{ hat keine Eigenwerte in }\mathbb{R}\\
  &\begin{array}{c@{,}r@{\Rightarrow}l}
     M = 
     \begin{bmatrix}
       0 & 1\\
       -1 & 0\\
     \end{bmatrix}
     \in \mathbb{C}^{2\times 2}
    & det(\lambda I - M) = \lambda^2 + i = 0
    & \lambda_1 = i, \lambda_2 = -i\cr 
    S = 
    \begin{bmatrix}
      -i & i\\ 
      1 & 1\\
    \end{bmatrix}
    & S^{-1} = \frac{1}{2}
      \begin{bmatrix}
        i & 1\\
        -i & 1\\
      \end{bmatrix}
    &S^{-1}MS = 
      \begin{bmatrix}
        i & 0\\
        0 & -i\\
      \end{bmatrix}
   \end{array}
\end{align*}
\end{enumerate}
\end{example}

\begin{theorem}
  Seine \(A, B \in K^{n \times n}\). Dann gilt: 
  \begin{enumerate}
    \item \(A^T\) hat die selben EW wie \(A\) 
    \item \(A\) hat den EW \(\lambda = 0 \Leftrightarrow A\) ist nicht
      invertierbar.
    \item \(A \cdot B\) und \(B \cdot A\) haben die selben EW
    \item Die Matrix \(A = 
      \begin{bmatrix} 
        a_{1\,1} & &*\\
        & \ddots & \\
        0 & & a_{n\,n}\\
      \end{bmatrix}
      \) hat die EW \(a_{1\,1},\hdots,a_{n\,n}\)
  \end{enumerate}
  \begin{proof}
    \begin{enumerate}
      \item \(0 = det(\lambda I - A) = det((\lambda I - A)^T) =
        det(\lambda I -A^T)\) 
      \item \(0 = det(0 I - A) = det(-A) = (-1)^n det(A)\)
      \item \(det(AB) = det(BA)\) ~\\
        Sei \(\lambda \neq 0\) ein EW von \(AB \Rightarrow ABv =
        \lambda v\) fr ein \(v \neq 0\) 
        \begin{align*}
          w=Bv 
          &\Rightarrow Aw = ABv = \lambda v\\
          &\Rightarrow w \neq 0\\
          &\Rightarrow BAw = BABv = \lambda Bv = \lambda w\\
          &\Rightarrow \lambda \text{ EW von } BA
        \end{align*}
      \item \(det(\lambda I - A) = det\left(
          \begin{bmatrix}
            \lambda a_{1\,1} & & *\\
            & \ddots & \\
            0 & & \lambda a_{n\, n}\\
          \end{bmatrix}
          \right)
          = (\lambda - a_{1\, 1}) \cdot \hdots \cdot (\lambda
          -a_{n\,n})
          \Rightarrow \lambda_j = a_{i\,j} \text{ für } j = 1, \hdots,
          n \)
    \end{enumerate}
  \end{proof}
\end{theorem}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_2"
%%% End:
